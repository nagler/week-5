import numpy as np


class Weights:
    def __init__(self, tensor):
        self.tensor = tensor
        self.gradient = np.zeros_like(self.tensor)


class GDOptimizer:
    def __init__(self, lr=0.0001):
        self.lr = lr

    def update(self, weights):
        weights.tensor -= self.lr * weights.gradient.T
        weights.gradient.fill(0)


class Layer:
    def __init__(self):
        self._in = None
        self._out = None

    def forward(self, x):
        self._in = x
        pass

    def backprop(self, dprev):
        pass

    def numeric_gradient(self, dprev):
        pass

#
# class Linear(Layer):
#     def __init__(self, input_shape, output_shape):
#         super(Linear, self).__init__()
#         self.weights = self.init_weights(input_shape, output_shape)
#
#     def init_weights(self, input_shape, output_shape):
#         pass
#
#     def update(self):
#         pass


def softmax(input):
    exp = np.exp(input)
    return exp / exp.sum(axis=1, keepdims=True)


class ReLU(Layer):
    def __init__(self):
        super(ReLU, self).__init__()

    def forward(self, x):
        super(ReLU, self).forward(x)
        self._out = np.maximum(0, self._in)
        return self._out

    def backprop(self, dprev):
        return False, dprev * (1. * (self._in > 0))
        #return False, np.dot(np.max(0, self._in > 0), dprev)


class NetworkGraph:
    def __init__(self, *layers):
        self.layers = layers[0]
        self.optimizer = GDOptimizer()
        self.output = None
        self.loss = None

    def forward(self, inputs):
        for layer in self.layers:
            inputs = layer.forward(inputs)
        self.output = softmax(inputs)
        return self.output

    def calculate_loss(self, y):
        self.loss = cross_entropy_loss(self.output, y)
        return self.loss

    def backprop(self, y):
        dprev = np.array(self.output - y)
        for layer in reversed(self.layers):
            Trainable, *dlayer = layer.backprop(dprev)
            if Trainable:
                self.optimizer.update(layer.weights)
            dprev = dlayer[0]
            pass

    def predict(self, inputs):
        return np.argmax(softmax(self.forward(inputs)), axis=1)

    def train(self, X, y, epochs=20000):
        losses = []
        for epoch in range(epochs):
            predict = self.forward(X)
            # feed forward
            self.backprop(y)
            if epoch % 10 == 0:
                loss = cross_entropy_loss(predict, y)
                losses.append(loss)
                print("Loss for epoch %i: %f" % (epoch, loss))

        return losses


def cross_entropy_loss(x, y):
    N = x.shape[0]
    return 1 - (np.sum(np.multiply(x, y)) / N)


