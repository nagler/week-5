import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
import tensorflow as tf
from sklearn.model_selection import train_test_split

# tf.enable_eager_execution()
tf.compat.v1.enable_eager_execution()


class DataTools(object):
    def __init__(self):
        pass

    @staticmethod
    def make(num_samples=1024, noise=0.2, display=False, split=False):
        # toy dataset
        X, y = datasets.make_moons(num_samples, noise=noise)
        y = DataTools.one_hot(y, np.max(y) + 1)
        if display:
            DataTools.plot(X, y)
        if split:
            xTrain, xTest, yTrain, yTest = train_test_split(X, y, test_size=0.2, random_state=42)
            return [xTrain, yTrain], [xTest, yTest]
        else:
            return [X, y]

    @staticmethod
    def one_hot(a, num_classes):
        return np.squeeze(np.eye(num_classes)[a.reshape(-1)])

    @staticmethod
    def plot(X, y):
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
        plt.show()

    @staticmethod
    def plot_decision_boundary(X, y, pred_func):
        # Set min and max values and give it some padding
        x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
        y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
        h = 0.01

        # y from onehot to index
        y = np.argmax(y, axis=1)

        # Generate a grid of points with distance h between them
        xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

        # Predict the function value for the whole gid
        Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
        Z = Z.reshape(xx.shape)

        # Plot the contour and training examples
        plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
        plt.show()


class NeuralWrapper(tf.keras.Model):
    """

    """
    def __init__(self, num_layers, hidden_sizes, activation=tf.nn.relu, use_dropout=False, split=True,
                 generate_data=True):
        """

        :param num_layers:
        :param hidden_sizes:
        :param activation:
        :param use_dropout:
        :param split:
        :param generate_data:
        """

        super(NeuralWrapper, self).__init__()

        if num_layers < 0:
            raise ValueError("Expected Positive number of layers.")

        if len(hidden_sizes) != num_layers:
            raise ValueError("Hidden sizes must match num_layers length.")

        self.layers_list = [tf.keras.layers.Dense(units=hidden_sizes[i],
                                                  activation=activation,
                                                  kernel_initializer='glorot_normal',
                                                  bias_initializer='zeros') for i in range(num_layers - 1)]
        self.layers_list.append(tf.keras.layers.Dense(units=hidden_sizes[-1],
                                                      activation=None,
                                                      kernel_initializer='glorot_normal',
                                                      bias_initializer='zeros'))
        self.data = None
        self.train = None
        self.test = None
        self.generate_data = generate_data
        self.use_dropout = use_dropout
        if self.use_dropout:
            self.dropout = tf.keras.layers.Dropout(0.5)  # To do: fix
        self.split = split
        self.compile(optimizer=tf.keras.optimizers.Adagrad(learning_rate=0.01),
                     loss=tf.losses.softmax_cross_entropy,
                     metrics=['accuracy'])
        pass

    def call(self, inputs):
        for layer in self.layers_list:
            inputs = layer(inputs)
        if self.use_dropout:
            inputs = self.dropout(inputs)
        return inputs

    def predict(self,
                x,
                batch_size=None,
                verbose=0,
                steps=None,
                callbacks=None,
                max_queue_size=10,
                workers=1,
                use_multiprocessing=False):
        """
        :Keras.Model.predict will return logits, so we make sure to return the label.
        """
        predictions = super(NeuralWrapper, self).predict(x)
        return np.argmax(predictions, axis=1)

    def load_data(self):
        if self.generate_data:
            # Generate a toy dataset (sklaern Moons)
            if self.split:
                self.train, self.test = DataTools.make(split=True)
                if not (self.train and self.test):
                    raise TypeError("Expected a list, got [None]")
            else:
                self.data = DataTools.make(split=False)
                if not self.data:
                    raise TypeError("Expected a list, got [None]")
        else:
            raise NotImplemented
            # To do: load a dataset from tf.flags.path


def main():
    # To do: use tf.flags and tf.summary
    Network_1 = NeuralWrapper(num_layers=3, hidden_sizes=[20, 20, 2], split=True)
    Network_1.load_data()
    Network_1.fit(x=Network_1.train[0], y=Network_1.train[1], epochs=500, batch_size=64, verbose=1,
                  validation_split=0.125)

    DataTools.plot_decision_boundary(Network_1.test[0], Network_1.test[1], lambda x: Network_1.predict(x))
    print(Network_1.evaluate(Network_1.test[0], Network_1.test[1]))


if __name__ == "__main__":
    main()
