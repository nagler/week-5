import keras
import tensorflow as tf


def build_layers(config, use_BN):
    layers = []
    for layer in config:
        if layer == "Maxpool":
            layers += [keras.layers.MaxPooling2D(pool_size=2, strides=2)]
        else:
            if not layers:
                conv = keras.layers.Conv2D(filters=layer, kernel_size=(3, 3), padding='same', input_shape=[32, 32, 3])
            else:
                conv = keras.layers.Conv2D(filters=layer, kernel_size=(3, 3), padding='same')
            if use_BN:
                layers += [conv, keras.layers.BatchNormalization(axis=-1, momentum=0.99, epsilon=0.001),
                           keras.layers.ReLU()]
            else:
                layers += [conv, keras.layers.ReLU()]
    layers += [keras.layers.Flatten()]
    return layers


def build_classification_layer():
    classification_list = [keras.layers.Dropout(0.2),
                           keras.layers.Dense(units=128,
                                              kernel_initializer='glorot_normal',
                                              bias_initializer='zeros'),
                           keras.layers.ReLU(),
                           keras.layers.Dropout(0.2),
                           keras.layers.Dense(units=128,
                                              kernel_initializer='glorot_normal',
                                              bias_initializer='zeros',
                                              name='features'),
                           keras.layers.ReLU(),
                           keras.layers.Dense(units=10,
                                              kernel_initializer='glorot_normal',
                                              bias_initializer='zeros',
                                              activation='softmax')]
    return classification_list


if __name__ == '__main__':
    pass
