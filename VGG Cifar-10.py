from __future__ import print_function
import keras
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Conv2D, MaxPooling2D
import os
import matplotlib.pyplot as plt
from vgg import *
from keras.utils import plot_model
from datetime import datetime
import numpy as np

# configure hyperparameters
batch_size = 32
num_classes = 10
epochs = 120
num_predictions = 20
save_dir = os.path.join(os.getcwd(), 'saved_models')
model_name = 'keras_cifar10_trained_model.h5'
log_dir = os.path.join(os.getcwd(), 'tf_logs')
if not os.path.isdir(save_dir):
    os.makedirs(save_dir)
if not os.path.isdir(log_dir):
    os.makedirs(log_dir)

# The data, split between train and test sets:
(x_train, y_train), (x_test, y_test) = cifar10.load_data()

class_names = """airplane
automobile
bird
cat
deer
dog
frog
horse
ship
truck""".split()

with open(log_dir + "/metadata.tsv", 'w') as f:
    f.write('\n'.join([class_names[int(y)] for y in y_test]))

print('x_train shape:', x_train.shape)
print(x_train.shape[0], 'train samples')
print(x_test.shape[0], 'test samples')

# show a sample image from the data

# Convert class vectors to binary class matrices. 
y_train = keras.utils.to_categorical(y_train, num_classes)
y_test = keras.utils.to_categorical(y_test, num_classes)
y_test

input_shape = x_train.shape[1:]

config = [32, 32, "Maxpool", 128, 128, "Maxpool", 256, 256, 256]
# model = VGG(build_layers(config, True))

# initiate RMSprop optimizer
opt = keras.optimizers.rmsprop(lr=0.0001, epsilon=1e-9)
# opt=keras.optimizers.RMSprop(lr=0.001, epsilon=1e-08, decay=0.0)
# Let's train the model using RMSprop
# model.compile(loss='categorical_crossentropy',
#               optimizer=opt,
#               metrics=['accuracy'])

tf_file_path = log_dir + "/" + datetime.now().strftime("%Y%m%d-%H%M%S")
# tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=tf_file_path)
reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='val_loss',
                                              factor=0.9,
                                              patience=5,
                                              min_lr=0.000001,
                                              min_delta=1e-5)
file_path = save_dir + "/loss-improvement-{epoch:02d}-{val_acc:.2f}.hd5"
checkpoint = keras.callbacks.ModelCheckpoint(file_path, monitor='val_acc', verbose=1, save_best_only=True,
                                             save_weights_only=False, mode='auto', period=1)
model = keras.models.Sequential()
layer_list = build_layers(config, True)
for layer in layer_list:
    model.add(layer)

for layer in build_classification_layer():
    model.add(layer)

datagen = tf.keras.preprocessing.image.ImageDataGenerator(
    featurewise_center=False,  # set input mean to 0 over the dataset
    samplewise_center=False,  # set each sample mean to 0
    featurewise_std_normalization=False,  # divide inputs by std of the dataset
    samplewise_std_normalization=False,  # divide each input by its std
    zca_whitening=False,  # apply ZCA whitening
    zca_epsilon=1e-06,  # epsilon for ZCA whitening
    rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    # randomly shift images horizontally (fraction of total width)
    width_shift_range=0.1,
    # randomly shift images vertically (fraction of total height)
    height_shift_range=0.1,
    shear_range=0.,  # set range for random shear
    zoom_range=0.,  # set range for random zoom
    channel_shift_range=0.,  # set range for random channel shifts
    # set mode for filling points outside the input boundaries
    fill_mode='nearest',
    cval=0.,  # value used for fill_mode = "constant"
    horizontal_flip=True,  # randomly flip images
    vertical_flip=True,  # randomly flip images
    # set rescaling factor (applied before any other transformation)
    rescale=None,
    # set function that will be applied on each input
    preprocessing_function=None,
    # image data format, either "channels_first" or "channels_last"
    data_format=None,
    # fraction of images reserved for validation (strictly between 0 and 1)
    validation_split=0.0)

datagen.fit(x_train)

model.compile(loss='categorical_crossentropy',
              optimizer=opt,
              metrics=['accuracy'])

# normalize data
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')


def normalize(X_train, X_test):
    # this function normalize inputs for zero mean and unit variance
    # it is used when training a model.
    # Input: training set and test set
    # Output: normalized training set and test set according to the trianing set statistics.
    mean = np.mean(X_train, axis=(0, 1, 2, 3))
    std = np.std(X_train, axis=(0, 1, 2, 3))
    X_train = (X_train - mean) / (std + 1e-7)
    X_test = (X_test - mean) / (std + 1e-7)
    return X_train, X_test


x_train, x_test = normalize(x_train, x_test)

# x_train /= 255
# x_test /= 255
tf_callback = tf.keras.callbacks.TensorBoard(log_dir=tf_file_path)

# fit the model on the training set
model.fit_generator(datagen.flow(x_train, y_train, batch_size=batch_size),
                    steps_per_epoch=x_train.shape[0] / batch_size,
                    epochs=epochs,
                    validation_data=(x_test, y_test),
                    shuffle=True,
                    callbacks=[reduce_lr, checkpoint, tf_callback])
plot_model(model, to_file='model.png')

# Save model and weights

# model_path = os.path.join(save_dir, model_name)
# model.save(model_path)
# print('Saved trained model at %s ' % model_path)

# Score trained model.
scores = model.evaluate(x_test, y_test, verbose=1)
print('Test loss:', scores[0])
print('Test accuracy:', scores[1])
