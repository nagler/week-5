import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from nn import *
from base_classes import *
from sklearn.preprocessing import LabelBinarizer


def plot_decision_boundary(X, y, pred_func):
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01

    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()


def make_classification_dataset(num_samples=1024, noise=0.1, display=False):
    # toy dataset
    X, y = datasets.make_moons(num_samples, noise=noise)

    if display:
        # visualize dataset
        plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
        plt.show()

    return X, y


def one_hot(a, num_classes):
  return np.squeeze(np.eye(num_classes)[a.reshape(-1)])

if __name__ == '__main__':
    X, y = make_classification_dataset()
    y_oneh = one_hot(y, 2)
    hidden_shape = 6
    output_shape = 2
    Layers = [Linear(X.shape[1], hidden_shape), BiasLayer(hidden_shape), ReLU(), Linear(hidden_shape, output_shape),
              BiasLayer(output_shape)]

    nn=NetworkGraph(Layers)
    nn.train(X, y_oneh)

    # predict on training set
    preds = nn.predict(X)
    print(accuracy_score(y, preds))

    # visualize decision boundary
    plot_decision_boundary(X, y, lambda x: nn.predict(x))
