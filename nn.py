from base_classes import *
import matplotlib.pyplot as plt
import numpy as np
from sklearn import datasets
from sklearn.metrics import accuracy_score

class Linear(Layer):
    def __init__(self, input_shape, output_shape):
        super(Linear, self).__init__()
        self.weights = self.init_weights(input_shape, output_shape)

    def init_weights(self, input_shape, output_shape):
        return Weights(np.random.randn(input_shape, output_shape))

    def update(self, dprev):
        self.weights.gradient = np.dot(np.array(dprev).T, self._in)

    def forward(self, x):
        super(Linear, self).forward(x)
        self._out = np.matmul(x, self.weights.tensor)
        return self._out

    def backprop(self, dprev):
        din = np.dot(np.array(dprev), self.weights.tensor.T)
        self.update(dprev)
        dW = np.dot(np.array(dprev).T, self._in)
        #self.update(dW)
        return True, din

    def numeric_gradient(self, dprev):
        delta = 0.001
        return dprev * ((self._out - np.matmul(self._in, np.add(self.weights.tensor, delta)))/delta)


class BiasLayer(Layer):
    def __init__(self, output_shape):
        super(BiasLayer, self).__init__()
        self.weights = self.init_weights(1, output_shape)

    def init_weights(self, input_shape, output_shape):
        return Weights(np.random.uniform(size=(input_shape, output_shape)))

    def forward(self, x):
        super(BiasLayer, self).forward(x)
        self._out = np.add(x, self.weights.tensor)
        return self._out

    def backprop(self, dprev):
        self.update(dprev)
        return True, dprev

    def update(self, dprev):
        self.weights.gradient = np.mean(dprev, axis=0)
#
# class Softmax(Layer):
#     def __init__(self):
#         super(Softmax, self).__init__()
#
#     def forward(self, x):
#         exp = np.exp(x)
#         self._out = exp / exp.sum(axis=1, keepdims=True)
#         return self._out
#
#     def backprop(self, dprev):
#         return False, dprev * self._out * (1-self._out)
#



